<?php	
		include_once("Skiclub.php");
		include_once("Skier.php");
		include_once("Log.php");
		
		function kjorAddSkiclub($PDO, $doc){		//Retrieves club data from XML file
		
		echo "Adding SkiClubs to database!". "\n\n";
			
		$xpath = new DOMXpath($doc);

		$elements = $xpath->query("/SkierLogs/Clubs/Club");
		
		foreach ($elements as $element) {		// For each element gets associated attribute and elements of child nodes
    
		$id = $element->getAttribute("id");
	  
	
		$name = $element->childNodes->item(1)->nodeValue. "\t";	
		$city = $element->childNodes->item(3)->nodeValue. "\t";
		$county = $element->childNodes->item(5)->nodeValue. "\t". "\n\n";
		
		$PDO->addSkiclub(new Skiclub($id, $name, $city, $county));
		}
	}
	
	
		function kjorAddSkier($PDO, $doc){			//Retrieves skier data from XML file
			
		echo "Adding Skiers to database!". "\n\n";	
		$xpath = new DOMXpath($doc);

		$elements = $xpath->query("/SkierLogs/Skiers/Skier");
		
		foreach ($elements as $element) {			// For each element gets associated attribute and elements of child nodes
    
			$username = $element->getAttribute("userName");
	  
	
			$firstname = $element->childNodes->item(1)->nodeValue;	
			$lastname = $element->childNodes->item(3)->nodeValue;
			$yearOfBirth = $element->childNodes->item(5)->nodeValue;
		
			$PDO->addSkier(new Skier($username, $firstname, $lastname, $yearOfBirth)); //Sends information to the database
			
			}
		}
		
		function kjorAddLog($PDO, $doc){			
		
		echo "Adding Logs for all skiers!". "\n\n";
			
		$xpath = new DOMXpath($doc);

		$elements = $xpath->query("/SkierLogs/Season");
		
			foreach ($elements as $element){
			
				$season = $element->getAttribute("fallYear");
				
		
				$nodes = $xpath->query("/SkierLogs/Season[@fallYear=$season]/Skiers");
		
				foreach ($nodes as $node){
					
						if($node->hasAttribute("clubId")){
							
						$clubid = $node->getAttribute("clubId");
						
						
						
						foreach ($xpath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[@clubId=\"$clubid\"]/Skier") as $skiers){
							$username = $skiers->getAttribute("userName");
							
							$distance = 0;
							
							foreach ($xpath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[@clubId=\"$clubid\"]/Skier[@userName =\"$username\"]/Log/Entry") as $skier){
								
								$distance += $skier->childNodes->item(5)->nodeValue; //Sums up the total distance for each skier
								 
							}
							
							$PDO->addLog(new Log($season, $username, $clubid, $distance));
						}
						}
						else{
							$clubid = NULL;
							
							foreach ($xpath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[not(@clubId)]/Skier") as $skiers){
							$username = $skiers->getAttribute("userName");
							
							$distance = 0;
							
							foreach ($xpath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[@clubId=\"$clubid\"]/Skier[@userName =\"$username\"]/Log/Entry") as $skier){
								
								$distance += $skier->childNodes->item(5)->nodeValue; //Sums up the total distance for each skier
								 
							}
							
							$PDO->addLog(new Log($season, $username, $clubid, $distance));
							}
						
				}
			}
		}
	}
?>