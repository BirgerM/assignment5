<?php

include_once("Skiclub.php");
include_once("Skier.php");
require_once("TestDBProps.php");


class DBModel
{        
    protected $db = null;  
    
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:dbname=assignment5;host='.TEST_DB_HOST,
                        				TEST_DB_USER, TEST_DB_PWD,
										array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
										
		}
    }
	
	public function addSkiclub($skiclub)		//Puts Skiclubs into the database
    {
		$sth = $this->db->prepare("INSERT INTO skiclubs(id, clubname, city, county) VALUES(:id, :clubname, :city, :county)");
		
		$sth->bindValue(':id', $skiclub->id, PDO::PARAM_STR);
		$sth->bindValue(':clubname', $skiclub->name, PDO::PARAM_STR);
		$sth->bindValue(':city', $skiclub->city, PDO::PARAM_STR);
		$sth->bindValue(':county', $skiclub->county, PDO::PARAM_STR);
		$sth->execute();
	
    }

	public function addSkier($skier){		//Puts skiers into the database
		$sth = $this->db->prepare("INSERT INTO skiers(username, firstname, lastname, yearOfBirth) VALUES(:username, :firstname, :lastname, :yearOfBirth)");
		
		$sth->bindValue(':username', $skier->username, PDO::PARAM_STR);
		$sth->bindValue(':firstname', $skier->firstname, PDO::PARAM_STR);
		$sth->bindValue(':lastname', $skier->lastname, PDO::PARAM_STR);
		$sth->bindValue(':yearOfBirth', $skier->yearOfBirth, PDO::PARAM_STR);
		$sth->execute();
			
	}
	public function addLog($log){
		$sth = $this->db->prepare("INSERT INTO season(fallyear, username, clubid, distance) VALUES(:fallyear, :username, :clubid, :distance)");
		
		$sth->bindValue(':fallyear', $log->season, PDO::PARAM_STR);
		$sth->bindValue(':username', $log->username, PDO::PARAM_STR);
		$sth->bindValue(':clubid', $log->clubid, PDO::PARAM_STR);
		$sth->bindValue(':distance', $log->distance, PDO::PARAM_STR);
		$sth->execute();
		
	}
}

?>