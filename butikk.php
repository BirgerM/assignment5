<?php
	//Filen som styrer alt - Butikk
	
	include_once("DBModel.php");
	include_once("DOMModel.php");
	
	$DBController = new DBmodel();
	
	if(!$DBController)
		echo "Connection Failed!". "\n";
	else{
		echo "Butikken er åpnet!". "\n";
		$file = "SkierLogs.xml";
		
		$doc = new DOMDocument();

		$doc->load($file);
		if($doc){
			echo "Nå snakker vi butikk!". "\n\n";
			
			kjorAddSkiclub($DBController, $doc);
			kjorAddSkier($DBController, $doc);
			kjorAddLog($DBController, $doc);
			
			echo "Complete";
		}
		else
			echo "Couldn't load file";
	}

?>