<?php
class Log {
	public $username;
	public $clubid;
	public $season;
	public $distance;


	public function __construct($season, $username, $clubid, $distance)  
    {  
        $this->username = $username;
        $this->clubid = $clubid;
	    $this->season = $season;
	    $this->distance = $distance;
    } 
}

?>